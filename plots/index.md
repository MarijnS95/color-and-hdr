---
SPDX-FileCopyrightText: 2022 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)

# Plots

Here are some Python scripts demonstrating various properties. The requirements
are:
- Python 3.9
- [NumPy](https://numpy.org/) >= 1.19 for numerical computing
- [Matplotlib](https://matplotlib.org/) >= 3.3 for plotting
<!-- vale proselint.Spelling = NO -->
- [Colour](https://www.colour-science.org/) >= 0.3.16 for color standards
<!-- vale proselint.Spelling = YES -->

The scripts are intended to be played with and modified. See the comments
inside each script for further discussion.

## Error from using a LUT to implement an inverse EOTF

### [transfer_function_lut1d_error.py](transfer_function_lut1d_error.py)

Graphs the error caused by the LUT approximation when encoding for
the power 2.2 transfer function.

![Transfer function and encoding error](images/transfer_function_lut1d_error.png "transfer_function_lut1d_error.py")

This makes it quite clear why inverse EOTF is much harder to
approximate with a 1D LUT than EOTF: near zero, inverse EOTF is very
steep yet it quickly becomes gradual. The same behavior is not observed
at the other end near one.

### [transfer_function_lut1d_error_taps.py](transfer_function_lut1d_error_taps.py)

Graphs the maximum absolute error of a LUT approximation over the whole domain
versus the number of taps in the LUT, for power-law 2.2 encoding transfer
function.

![Maximum absolute error vs. LUT taps](images/transfer_function_lut1d_error_taps.png "transfer_function_lut1d_error_taps.py")

This shows that increasing the number of taps has diminishing returns,
especially for pure power-law inverse functions, while the approximation
error remains relatively high even for 8-bit values.

## Visualization

### [primary_container_target_color_volumes.py](primary_container_target_color_volumes.py)

Graphs examples of the Primary Color Volume, the Container Color Volume and the
Target Color Volume projected onto the CIE xy plane. Volumes are three
dimensional but the projection is enough to show how the volumes can be
different.

![Primary, Container and Target Color Volumes](images/primary_container_target_color_volumes.png "primary_container_target_color_volumes.py")

The primary color volume spans the area between the primaries of the color
space. The container color volume usually is either the same as the primary
color volume or practically infinite. The target color volume describes all the
colors that the content might use and can be any volume that's encompassed by
the container color volume. It can be either bigger than the primary color
volume to describe more colors (for example scRGB; sRGB primaries and a floating
point pixel format) or smaller than the primary color volume to minimize tone
and gamut mapping distances (for example via HDR metadata).
