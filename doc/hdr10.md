---
SPDX-FileCopyrightText: 2023 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)


# How to build a HDR10 system using Wayland

This is a recipe for building a display system that can take and output
HDR10 streams. Most importantly:
- No component needs a CMM or CMS (color management module or system).
- No component needs to deal with ICC files.
- It is still standard Wayland, based on [the color-management draft][MR14].

It is possible to use a CMM, support ICC files, and implement a lot more
features if desired, but they are **not required for HDR10 support**.

The main caveats are:
- You cannot really forbid clients from using legacy untagged content (no color
  information provided), and that content is almost guaranteed to be SDR and
  very close to sRGB. You should handle that somehow.

You can use the same principle to craft a Wayland display system that supports
any other HDR specification without the need for ICC or CMM.

[MR14]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14


## HDR10 Media Profile

The HDR10 Media Profile specification can be found in [Wikipedia][wiki-HDR10].
Unfortunately the main reference for the definition can only be seen through
[web archives][ref-HDR10]. Another source is an arbitrary
[PDF document][unknown-HDR10].

To repeat that definition:
- EOTF: SMPTE ST 2084
- Color Sub-sampling: 4:2:0 (For compressed video sources)
- Bit Depth: 10 bit
- Color Primaries: ITU-R BT.2020
- Metadata: SMPTE ST 2086, MaxFALL, MaxCLL

In other words, it uses BT.2100/PQ system with the HDR static metadata and
MaxFALL and MaxCLL included. However, it also demands
- 10 bits per channel color depth, and
- YCbCr or similar pixel format, because of
- 4:2:0 chroma sub-sampling.

The metadata is listed in [CTA-861.3-A] which is available to everyone free of
charge after registering. It also defines the algorithms to compute MaxFALL and
MaxCLL.

Therefore the HDR10 Media Profile needs to be handled in parts:
- [color-management][MR14] takes care of the color space, transfer function,
  dynamic range, and HDR static metadata.
- [color-representation][MR183] takes care of YUV-RGB conversion, chroma
  sub-sampling details, and straight alpha (it was not listed, but you really
  want it for performance).
- [linux-dmabuf] takes care of the 10 bits-per-channel pixel formats. (Don't
  worry, the extension is not unstable anymore, it was just left in that
  directory for historical reasons.)

[wiki-HDR10]: https://en.wikipedia.org/wiki/HDR10
[ref-HDR10]: https://web.archive.org/web/20190611151620/https://www.cta.tech/News/Press-Releases/2015/August/CEA-Defines-%E2%80%98HDR-Compatible%E2%80%99-Displays.aspx
[unknown-HDR10]: https://cdn.cta.tech/cta/media/media/membership/pdfs/video-technology-consumer-definitions.pdf
[CTA-861.3-A]: https://shop.cta.tech/products/hdr-static-metadata-extensions
[MR183]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/183
[linux-dmabuf]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/tree/main/unstable/linux-dmabuf


## Compositor implementation of HDR10

A compositor needs to implement the following Wayland protocols:
- color-management
  - Request `wp_color_manager_v1.get_color_management_surface`.
    - Interface `wp_color_management_surface_v1` in full.
  - Request `wp_color_manager_v1.get_color_management_output`.
    - Interface `wp_color_management_output_v1` in full.
  - Interface `wp_image_description_v1`
    - The compositor never needs to send the `icc_file` event.
  - Request `wp_color_manager_v1.new_parametric_creator` and
    advertise `wp_color_manager_v1.feature.parametric`.
  - Request `wp_image_description_creator_params_v1.set_tf_cicp` and
    advertise CICP TransferCharacteristic 16 with `wp_color_manager_v1.supported_tf_cicp`.
  - Request `wp_image_description_creator_params_v1.set_primaries_cicp` and
    advertise CICP ColourPrimaries 9 with `wp_color_manager_v1.supported_primaries_cicp`.
  - Mastering (target color volume) metadata requests in
    `wp_image_description_creator_params_v1` and their respective advertisements.
  - Destroy requests for everything.
  - Any other request can raise the appropriate protocol error as specified.
- color-representation
  - All of it for HDR10, or if you support only RGB then only the alpha mode
    by advertising no supported matrix coefficients or chroma locations.
- linux-dmabuf
  - All of it.

The global interface `wp_color_manager_v1` needs to advertise only the
perceptual rendering intent. Perceptual rendering intent is the one that allows
you to do anything you like to colors in an attempt to convey the intended
perception. It does not need to be a static color mapping either, it can be as
dynamic as you want it.

The global interface `wp_color_manager_v1` allows clients to create image
description objects for the compositor's preferred color space on a
`wl_surface`, and the image description of a `wl_output`. You must implement
this. The surface's preferred image description is where you can recommend
clients to use a specific set of parameters. The output's image description
should describe the output you are driving regardless of your preferences, but
also clients are normally expected to not use that for producing their content.

HDR static metadata is included through `wp_image_description_creator_params_v1`.
If your compositor does any kind of composition, it must be prepared to compose
client contents that have differing HDR metadata. How well you do that is up to
you, image quality wise.

Color-representation allows you to choose the CICP
*MatrixCoefficients*+*VideoFullRangeFlag* and *Chroma420SampleLocType* you want
to support, but you have to support all combinations of any individual code
points you do support. It also requires support for both pre-multiplied and
straight alpha. As color-representation is independent of color-management,
again you need to support all possible combinations. As said before, how well
you do that is up to you.

If you really do want to restrict clients to 10 bits-per-channel YUV 4:2:0 only
as per HDR10 Media Profile, you can do that by not advertising any other pixel
formats via linux-dmabuf. That may exclude the majority of applications, though.
Likewise, you could choose to not support `wl_shm` buffer factory.

### scRGB

If you want to support delivery of BT.2020 color gamut encoded in scRGB using a
linear transfer function:
- Advertise CICP TransferCharacteristic 8 with `wp_color_manager_v1.supported_tf_cicp`.
- Advertise CICP ColourPrimaries 1 with `wp_color_manager_v1.supported_primaries_cicp`.
- Advertise `wp_color_manager_v1.feature.extended_target_volume`.

Clients will communicate the BT.2020 color gamut with
`set_mastering_display_primaries` while choosing the linear transfer
characteristic and BT.709 primaries and white point. Clients will also use a
floating-point pixel format. The compositor must handle arbitrary (negative
and greater than 1.0) color channel values.

How to indicate the dynamic range is **to be designed**. E.g. how to indicate
that scRGB optical (1.0, 1.0, 1.0) matches PQ-encoded 80 cd/m² in which one of
the viewing environments referred by SMPTE ST 2084.
[Discussion.](https://gitlab.freedesktop.org/pq/color-and-hdr/-/issues/23)


## Client implementation of HDR10

Always use the CICP enumerations when possible to ensure compositor support.

### GPU rendering applications

The main part of the client implementation will be in Mesa. Application code
does not necessarily need to interface with color-management protocol at all.
Instead, applications use interfaces like [VkColorSpaceKHR] and
[EGL_EXT_gl_colorspace_bt2020_pq].

[VkColorSpaceKHR]: https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VkColorSpaceKHR.html
[EGL_EXT_gl_colorspace_bt2020_pq]: https://registry.khronos.org/EGL/extensions/EXT/EGL_EXT_gl_colorspace_bt2020_linear.txt

### Non-GPU video applications

These applications will probably need to manually use the same interfaces as
compositors are instructed to implement above.

### Adaptive applications

Applications that can adapt to the compositor recommendations should react to
`wp_color_management_output_v1.image_description_changed` with the
`get_image_description` request, and inspect the resulting image description
object with `wp_image_description_v1.get_information`.

Whether this is abstracted by any library API like EGL or Vulkan is up to
that API.
