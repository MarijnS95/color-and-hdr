---
SPDX-FileCopyrightText: 2022 François-Xavier Thomas <fx.thomas@gmail.com>
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)


# Tools

This page lists software tools and commands that can be used to inspect or
modify various aspects of display and color management on Linux.


## Display capabilities

List information communicated by the display via EDID (using [edid-decode]):

    edid-decode < /sys/class/drm/<name>/edid

List information communicated by the display via DDC (using [ddcutil]); not all
displays support this):

    ddcutil capabilities

[edid-decode]: https://git.linuxtv.org/edid-decode.git
[ddcutil]: https://www.ddcutil.com/


## Color management

Dump all information [colord] is aware of, including a list of profiles stored
on the system:

    colormgr dump

Dump the contents of an ICC profile (part of colord):

    cd-iccdump <file.icc>

Some GUI tools also exist:

* **gcm-viewer**: inspects ICC profiles stored on the system (from
  [gnome-color-manager])
* [DisplayCal] can also display information about an ICC profile (in File >
  Profile Information)

[colord]: https://www.freedesktop.org/software/colord/index.html
[gnome-color-manager]: https://gitlab.gnome.org/GNOME/gnome-color-manager
[DisplayCal]: https://displaycal.net/


## Configuration (X11)

Dump what RandR knows about all outputs, which includes a lot of information
about curves/color:

    xrandr --query --verbose

Dump the current VCGT Intensity + RGB tables for display \#1 (using [ArgyllCMS]):

    dispwin -d1 -s current_vcgt.cal

Display/change the current X11 "gamma" setting (not sure where that is in
comparison with VCGT, it does not seem to change it according to `dispwin`
above):

    xgamma
    xgamma -gamma x.y

Display/change the RandR "gamma" setting (this one does seem to change the VCGT,
but only RGB components, not the intensity):

    xrandr --output eDP1 --gamma x.y

Commands like this one are related to KMS properties that tag the HDMI output
with specific information using AVI InfoFrames described in CTA-861-H 6.4.2
"Color Component Sample Format and Colorimetry". This is useful for some
displays that have specific built-in calibrations, and can understand that the
incoming HDMI signals are in, say, Rec.2020 provided they have the correct
metadata. Note that not all drivers implement all color-related KMS properties
(most of them being documented in [Standard Connector Properties][KMS-props].

    xrandr --output eDP1 --set Colorspace BT2020_RGB

[ArgyllCMS]: https://www.argyllcms.com/
[KMS-props]: https://www.kernel.org/doc/html/latest/gpu/drm-kms.html


## Configuration (Wayland)

On Wayland most configuration tools would be compositor-specific.

The plan is that there will be generic tools to inspect what the compositor says
about the displays and supported color capabilities, as well as an interface for
profiling displays and possibly a method for proposing a new display profile to
the compositor.


## Internal and low-level utilities

* **proptest** inside **libdrm** allows to dump and modify internal properties
  known to DRM inside the kernel (packages for:
  [Arch](https://aur.archlinux.org/packages/libdrm-proptest),
  [Debian](https://packages.debian.org/bullseye/libdrm-tests)).

* [drm_info] can only be used for inspecting the current DRM configuration, and
  can output the report as JSON for further processing (e.g. feeding into the
  [DRM database]).

[drm_info]: https://github.com/ascent12/drm_info
[DRM database]: https://drmdb.emersion.fr
