---
SPDX-FileCopyrightText: 2023 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)

<!-- Color vs Colour, but they are quotes -->
<!-- vale proselint.Spelling = NO -->


# How to learn about digital color

This walk-through of recommended reading is an introduction into digital color
for someone who has **zero previous knowledge** or experience on the subject.

The aim is to let the reader acquire an intuition of what is color, how color
is defined digitally, and why do we need complicated things like color
management. This is intended to help understand the proposed Wayland color
features. The idea is to digest the material roughly from top to bottom.

Please, take your time. Read, ponder, skip, backtrack, repeat, reflect.
Pace yourself to not be overwhelmed as the subject is wide and deep. Pick
interesting topics and do not worry if you do not understand everything on
the first read. Read something else, and revisit a topic later.


## Physics vs. human vision

### [The Dimensions of Colour](http://www.huevaluechroma.com/)

This site has collected a good deal of the modern understanding of color in a
single, consistent and readable set of pages with many illustrations.
It covers color in much wider scope than most presentations, which is essential
for getting a good overall idea. Naturally, it is also a long read, but it is
worth it.

If you cannot spare the time or patience right now, check the below Wikipedia
entries, and come back later.

If you want to avoid misunderstandings from the beginning it would be best to
not skip this. You would miss most aspects of color and vision.

### [Wikipedia: Color](https://en.wikipedia.org/wiki/Color)

The main take-aways from this introduction are:
- In physics, light has a power distribution over wavelengths.
- Human eyes do not measure light spectrum wavelength by wavelength, instead
  they have different kinds of photosensitive cells that have different
  spectral sensitivities across the whole light spectrum.
  Roughly, each type produces a different scalar signal, a component of color,
  which is then further processed in the eye before sent into the brain.
- Usually when we talk about color, we are interested in the three particular
  types of photosensitive cells (cones), and ignore the others.
- The above choice means there needs to be a good amount of light. Cones do not
  work in very low levels of light.
- The chosen three scalar signals are denoted by L, M, and S. Grossly
  simplified, you could think of those as red, green, and blue color components.
  **Note!** Please, read "The YouTube Theory of Colour Vision" below before
  you make any further conclusions from this.
- None of this implies what a color stimulus (light coming into an eye)
  *looks like* (perception). What happens in the brain can change everything.

That last point is important. See the [illusion]. The light spectra emitted by
your monitor from the two squares are identical, yet you quite probably see them
as different colors - within the same picture even. With a quick internet search
for "color illusion", you can find many examples where the color is the same but
you perceive a difference that is not physically there, or that a color looks
something it physically is not.

Therefore, while physical measurement of color [colorimetry] is very useful and
even necessary, providing some concept of measurable differences without which
making predictable display systems would not be possible, it is only a fraction
of the truth.

[illusion]: https://en.wikipedia.org/wiki/Checker_shadow_illusion
[colorimetry]: glossary.md#colorimetry

### [The YouTube Theory of Colour Vision](http://hueangles.blogspot.com/2018/08/the-youtube-theory-of-colour-vision.html)

Simplifications are always somewhat inaccurate if not outright incorrect, and
making conclusions based on simplifications can set you on a wrong track. This
post points out popular misconceptions about color vision.

### [Wikipedia: Metamerism (color)](https://en.wikipedia.org/wiki/Metamerism_(color))

The article mostly talks about objects vs. illumination. With light emitting
displays we do not have an object reflecting light but there is a simpler form
of metamerism in action: two different light spectra can produce the same
response in the human visual system (eyes + brain).

Metamerism is what allows us to build usable color displays where images
resemble reality. We do not need spectral cameras and spectral displays to
achieve resemblance of colors seen in the wild, we only need to cause a similar
response in the human visual system. That is why we get so far with only three
color channels in digital images and electronic displays.


## Digital color

### [The Hitchhiker's Guide to Digital Colour](https://hg2dc.com/)

<!-- usage of `clearly` is acceptable here -->
<!-- vale alex.Condescending = NO -->
This explains the basics of digital color really slowly and clearly, targeting
an audience that has used any numerical color picker in any drawing or other
application. The reader needs no other experience to grasp this. That makes
this well suited also for display server developers oblivious to digital color.
A good and long read, in spite of (or thanks to?) its "totally unprofessional
tone". Start from Question 1 and proceed in order.
<!-- vale alex.Condescending = YES -->

### [A Pixel's Color](pixels_color.md)

If you read through The Hitchhiker's Guide to Digital Colour, this article
has little new to you. This is largely a summary of the Hitchhiker's Guide,
although written completely independently and with more focus for Wayland
compositor and application toolkit developers as the audience.

### [Color Spaces - Bartosz Ciechanowski](https://ciechanow.ski/color-spaces/)

The goal of this blog post is to explain what defines a color space:
tone response curves (or color component transfer functions), primaries,
and white point.

The first part concentrates on the algebra of RGB color mixing and the
conversion between two arbitrary RGB color spaces. A dry topic is made more
interesting with **interactive diagrams**.

The second part explains how the famous CIE 1931 xy chromaticity diagram came
to be by explaining the CIE 1931 XYZ color space and how that is anchored to
the physics of light. CIE 1931 XYZ is a popular reference color space through
which other RGB color spaces can be defined.

### [Frequently Asked Questions about Gamma](https://poynton.ca/GammaFAQ.html)

Questions and short answers by Dr. Charles Poynton on topics of intensity
coding, video systems, and their relationship to viewing conditions.

Mind, this was written in the 1990s.

### [Frequently Asked Questions about Color](https://poynton.ca/ColorFAQ.html)

Questions and short answers by Dr. Charles Poynton on topics of color image
coding, computer graphics, image processing, video, and the transfer of digital
images to print.

Mind, this was written in the 1990s.


## Color management

### [The Color of Toast](http://www.colorwiki.com/wiki/The_Color_of_Toast)

This short write-up may be the simplest analogue of why do we need color
management and what it does.

<!-- proselint doesn't like the term `Hopefully` -->
<!-- vale proselint.Skunked = NO -->
### [Profiling Your Monitor — Popular Confusions, Hopefully Cleared](https://ninedegreesbelow.com/photography/monitor-profile-calibrate-confuse.html)
<!-- vale proselint.Skunked = YES -->

Profiling and calibrating a monitor are actually two very distinct matters.
This article explains the difference.


## Viewing environments and viewer adaptation

- to do


## High dynamic range (HDR)

### [HDR - An Introduction](https://web.archive.org/web/20231112101348/https://www.finalcolor.com/colorblog/hdr-an-introduction)

This post starts with disambiguation: multiple things are referred to as HDR,
HDR displays are just one of them. The writer is a professional colorist which
gives the introduction to HDR displays a nice, less technology-babble, tone.

### [High Dynamic Range (HDR)](https://tftcentral.co.uk/articles/hdr)

This is another HDR introduction that digs into the consumer markets and the
pieces of technology involved.

### [Why Your HDR Monitor is (Probably) Not HDR at All – and Why DisplayHDR 400 Needs to Go](https://tftcentral.co.uk/articles/why-your-hdr-monitor-is-probably-not-hdr-at-all-and-why-displayhdr-400-needs-to-go)

Things to note if you are looking to buy an HDR monitor, marketing can be
misleading.

### [High Dynamic Range Television and Hybrid Log-Gamma](https://www.bbc.co.uk/rd/projects/high-dynamic-range)

BBC Research & Development page has some nice links to explain things related
to HDR. BBC and NHK developed the Hybrid Log-Gamma (HLG) system for
broadcasting HDR television content.

The [BBC HDR TV FAQ] works as a short introduction to why one would want HDR and
what it is.

[BBC HDR TV FAQ]: https://downloads.bbc.co.uk/rd/pubs/papers/HDR/BBC_HDRTV_FAQ.pdf


## Other resources

### [The Khronos Dataformat Specification](https://www.khronos.org/dataformat)

This specification covers many things. Chapter *III Color conversions*
(from version 1.3.1, 2020-04-03) is relevant to digital color. That chapter is
more of a handbook of the technical details of various [ITU] publications than
learning material.

[ITU]: https://www.itu.int

### [W3C Workshop on Wide Color Gamut and High Dynamic Range for the Web](https://www.w3.org/Graphics/Color/Workshop/overview.html)

Various presentations (slides, video, transcriptions) and a summary report.
Topics include wide color gamut (WCG), HDR, CSS, compositing, tone mapping,
content creation (Krita), hybrid log-gamma (HLG), OKLab color space, iccMAX.

### [High Dynamic Range and Wide Gamut Color on the Web](https://w3c.github.io/ColorWeb-CG/)

An internal W3C document, a gap analysis of what is going to be needed to
have WCG and HDR in the web.

The list of references is interesting, [Principles of HDR in Chrome] for
instance.

[Principles of HDR in Chrome]: https://w3c.github.io/ColorWeb-CG/#bib-hdr-chrome
