---
SPDX-FileCopyrightText: 2020-2023 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)

<!-- markdownlint-disable heading-increment -->


# Glossary

#### alpha

A value that denotes the portion of a pixel covered by the pixel's color. When
not 100%, the remainder of the pixel is transparent. Sometimes used for
translucency instead.

#### calibration

Calibration is the fixed behavior of an output in response to its input signal.
The same input signal must always result in the same output, e.g. light emission
in displays.

When you calibrate a device or a system, you change its behavior.

#### color channel

One of the dimensions in a color model. For example, RGB model has channels R,
G, and B. [Alpha](#alpha) channel is sometimes counted as a color channel as
well.

#### color effect

Color effect is any manipulation of [colorimetry](#colorimetry) in a way that is
not part of display [calibration](#calibration) and is not warranted by the
input and output image descriptions under the chosen rendering intent.

#### color stimulus

Light entering a human eye, often quantified by [colorimetry](#colorimetry)
rather than radiometric power distribution over the visible light spectrum.

The sensation of color is affected by great many other things as well than
only the color stimulus, and even then the sensation is ultimately subjective.

<!-- vale proselint.Spelling = NO -->
Definition in e-ILV: [colour stimulus][e-ILV:colour stimulus]
<!-- vale proselint.Spelling = YES -->

#### color value

A tuple of numbers, representing a [color stimulus](#color-stimulus) in some
color system. Each element in the tuple is a [color channel](#color-channel)
value.

#### color volume

Short for [colorimetric](#colorimetry) volume.

A three dimensional space of [color value](#color-value)s. This includes both
"color" (e.g. hue and saturation) and brightness (luminance).

#### colorimetry

Colorimetry is the science of measuring visible radiation
([color stimuli](#color-stimulus)), assumed to enter the human eye, and
attempting to quantify the "color" of that radiation.

The experience of color is a combination of physical and psychological
phenomena. As colorimetry is the measurement of color stimuli, or light, it
considers only the physical aspect of the color experience. Therefore it does
not measure color *appearance* (what it actually looks like).

The result of a colorimetric measurement is usually a vector of three
[tristimulus values](#tristimulus-values). A tristimulus vector can be uniquely
computed from a light spectrum, but a specific tristimulus vector could be the
result from an infinite number of different light spectra.

#### compromised color reproduction

Color reproduction is *compromised*, if the display [calibration](#calibration)
changes without end user agreement, or any [color effect](#color-effect) is
applied.

This can be extended to include viewing conditions as well.

#### container color volume

This is the [color volume](#color-volume) addressable by a given signal
container format ([pixel format](#pixel-format), encoding, defining color
space). An example is BT.2020/PQ as is standard for driving many consumer HDR
monitors.

As a special case, the container color volume with floating-point
[pixel format](#pixel-format)s is practically infinite, because negative and
greater than 1.0 values can be represented. The defining color space and dynamic
range are likely anchored to channel values 0.0 and 1.0, but due to
extrapolation the representable color volume is not limited by these. The target
color volume needs to be specified separately.

See also: [target color volume](#target-color-volume),
[diagrams][plots:color-volumes]

#### image

Conceptually a two-dimensional array of [pixel](#pixel)s. The pixels may be
stored in one or more [memory buffer](#memory-buffer)s. Has width and height in
pixels, a [pixel format](#pixel-format) and a [modifier](#modifier) (implicit
or explicit).

#### linear values, light-linear

See [tristimulus](#tristimulus-values).

#### memory buffer

A piece of memory for storing (parts of) [pixel data](#pixel-data). Has stride
and size in bytes and at least one handle in some [API](#api). Contains one or
more [planes](#plane).

#### modifier

A description of how [pixel data](#pixel-data) is laid out in
[memory buffer](#memory-buffer)s.

For examples, see [drm_fourcc.h] of the Linux kernel.

#### nit

An absolute unit of luminance: cd/m², candelas per square meter. Candela is a
measure of luminous intensity, accounting for the human eye sensitivity to
light.

#### pitch

See [stride](#stride).

#### pixel

A picture element. Has a single [color value](#color-value) which is one or more
[color channel](#color-channel) values, e.g. R, G and B, or Y, Cb and Cr. May
also have an [alpha](#alpha) value as an additional channel.

#### pixel data

Bytes or bits that represent some or all of the color/alpha channel values of a
[pixel](#pixel) or an [image](#image). The data for one pixel may be spread over
several [plane](#plane)s or [memory buffer](#memory-buffer)s depending on
[format](#pixel-format) and [modifier](#modifier).

#### pixel format

A description of how [pixel data](#pixel-data) represents a pixel's
[color](#color-value) and [alpha](#alpha) values.

#### plane

In [image](#image) storage:

* A two-dimensional array of some or all of an [image](#image)'s
[color](#color-value) and [alpha](#alpha) values.

In [DRM](#drm) [KMS](#kms):

* A hardware composition element capable of holding one framebuffer
([image](#image)) and having position and stacking order on screen.

#### primary color volume

This is the [color volume](#color-volume) reachable by all
[tristimulus values](#tristimulus-values) where each component has a value
between 0.0 and 1.0, inclusive.

In most cases this is the same as the
[container color volume](#container-color-volume). One notable exception are
container color volumes with floating-point [pixel format](#pixel-format)s.

See also: [target color volume](#target-color-volume),
[diagrams][plots:color-volumes]

#### stride

Usually the offset from the beginning of one row of [pixels](#pixel) to the
beginning of the very next row in an [image](#image) in memory. Depending on the
API, can be in units of either bytes or pixels.

When the pixel layout is not linear, a more general definition can be used. If a
[modifier](#modifier) defines a block-based layout, stride is the offset in
bytes from the beginning of one block-row to the beginning of the very next
block-row divided by block height in pixels.

Stride is fundamentally different from width in that pixel rows can contain
padding that does not carry [pixel data](#pixel-data). Width is the number of
pixels on a row, and stride is the amount of memory used by the row.

#### target color volume

The [color volume](#color-volume) intended to be usable by a given video or image
signal. That is, the signal has been produced targeting this color volume in the
mastering process. An example is using HDR static metadata (SMPTE ST 2086) to
describe the image content when the container is BT.2020/PQ.

Traditional container formats and ICC v2-v4 profiles assume that container color
volume equals target color volume. This makes the most use of the pixel storage.
Using target color volume smaller than the container color volume means that
some code points will never be used, which is wasteful for storage.

See also: [container color volume](#container-color-volume),
[diagrams][plots:color-volumes]

#### trichromatic system

A trichromatic system defines the three primary colors and their relative
luminance (the white point). The system produces a range of colors through a
linear combination of its primaries, e.g. in a display by controlling the
luminance of the red, green and blue sub-pixel light sources (without changing
the chromaticity of each light source).

The primary colors do not necessarily need to be real. Imaginary primaries can
be used for calculations.

Definition in e-ILV: [trichromatic system][e-ILV:trichromatic system]

#### tristimulus values

<!-- usage of `simply` is acceptable here -->
<!-- vale alex.Condescending = NO -->
A tuple of three tristimulus values in some
[trichromatic system](#trichromatic-system). Each of the three values is
directly proportional to the [luminance](https://en.wikipedia.org/wiki/Luminance)
of its respective primary color. This direct proportionality is casually
referred to as the (tristimulus) values being light-linear or simply linear.
<!-- vale alex.Condescending = YES -->

A tuple in a given trichromatic system defines a color as in
[colorimetry](#colorimetry): a measured chromaticity and luminance of light.
This is a physical definition of color, derived from human perception in certain
strictly controlled environment and test setup. As such, it does not include the
dynamic adaptation or the psychological effects in the human visual system.

The tristimulus values of a color can be converted from one trichromatic system
to another with a 3×3 matrix multiplication. This is equivalent to a change of
basis in linear algebra, it is not color gamut mapping.

Definition in e-ILV: [tristimulus values][e-ILV:tristimulus values]

#### Wayland

A window system protocol.

#### X11

A window system protocol.


# Acronyms

#### API

application programming interface

#### AVI

auxiliary video information (InfoFrame)

#### CM

color management

#### CMM

color management module

#### CRTC

cathode-ray tube controller, nowadays a hardware block or an abstraction that
produces a timed stream of raw digital video data

#### DRM

direct rendering manager

#### EOTF

electro-optical transfer function

#### HDR

high dynamic range

#### KMS

kernel modesetting, display driver kernel-userspace API

#### LUT

look-up table

#### OETF

opto-electrical transfer function

#### SDR

standard dynamic range

#### VCGT

video card gamma table, a non-standard tag added into ICC profiles originally by
Apple

[e-ILV:tristimulus values]: https://cie.co.at/eilvterm/17-23-038
[e-ILV:trichromatic system]: https://cie.co.at/eilvterm/17-23-036
[e-ILV:colour stimulus]: https://cie.co.at/eilvterm/17-23-002
[plots:color-volumes]: ../plots/index.md#primary_container_target_color_volumespy
[drm_fourcc.h]: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/uapi/drm/drm_fourcc.h?h=v6.0#n362
