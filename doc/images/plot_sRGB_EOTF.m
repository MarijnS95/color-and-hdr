% SPDX-FileCopyrightText: 2021 Collabora, Ltd.
% SPDX-FileCopyrightText: 2022 Red Hat
% SPDX-License-Identifier: MIT

% This is an Octave script: https://www.gnu.org/software/octave/

e = [0 : 0.01 : 1];

o = zeros(size(e));

o = realpow(e, 2.2);

f = figure();
plot(e, o);
xticks([0:0.1:1])
yticks([0:0.1:1])
grid on
title('sRGB Display EOTF (pure power-law 2.2)')
xlabel('electrical / non-linear value')
ylabel('optical / linear value')
axis square tight

ppi = get(0, 'ScreenPixelsPerInch');
set(f, 'PaperPosition', [0 0 400 400] ./ ppi)

print(f, 'sRGB_EOTF.png', '-dpng')
