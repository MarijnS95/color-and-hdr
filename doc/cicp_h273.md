---
SPDX-FileCopyrightText: 2022 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)


# Coding-independent code points for video signal type identification

[Recommendation ITU-T H.273][H.273] *Coding-independent code points for video
signal type identification* defines code points (enumeration values) for some
properties of digital video imagery:
- **ColourPrimaries**: RGB color primaries and white point
- **TransferCharacteristics**: transfer characteristics, a.k.a transfer
  function or "gamma"
- **MatrixCoefficients**: conversion between YCbCr (and similar) and RGB
  color models
- **VideoFullRangeFlag**: quantization range; the flag indicates full range
  as opposed to limited range
- **VideoFramePackingType** and **QuincunxSamplingFlag**: how a stereo image
  pair (3D video content) is stored spatially and temporally
- **PackedContentInterpretationType**: which image is left or right in a stereo
  image pair
- **SampleAspectRatio, SarWidth, SarHeight**: sample (pixel) aspect ratio
- **Chroma420SampleLocType**: chroma siting for 4:2:0 sub-sampling

The idea is that these enumerations and flags can be used by other
specifications directly, and avoid having to define the same enumerations
over and over again.

This article explains and derives from Rec. ITU-T H.273. This article is
informational, while Rec. ITU-T H.273 is authoritative.

[H.273]: https://handle.itu.int/11.1002/1000/14661


## Decoding of TransferCharacteristics

<!-- usage of `easy` is acceptable here -->
<!-- vale alex.Condescending = NO -->
The table of `TransferCharacteristics` in Rec. ITU-T H.273 records a
mathematical equation for many of the code points, with the introduction saying
that they are either OETF or EOTF⁻¹. This is the transfer characteristic that is
theoretically used when encoding color into pixels. Unfortunately, it is
very common that the proper decoding procedure is not the inverse of the encoding
procedure, because the video system is designed to apply a non-linear curve to
improve the image on a display. Therefore as a Wayland compositor developer it
is easy to be mislead by H.273.
<!-- vale alex.Condescending = YES -->

The following table of `TransferCharacteristics` code points attempts to
highlight the differences:

| code | equation in H.273 | encoding practice | decoding practice |
|------|-------------------|-------------------|-------------------|
| 0    | "reserved" | - | - |
| 1    | BT.709-6 OETF | BT.709-6 OETF + adjustments | BT.1886 EOTF |
| 2    | "unspecified" | - | - |
| 3    | "reserved" | - | - |
| 4    | "Assumed display gamma 2.2" | ? | $`o = e^{2.2}`$? (6) |
| 5    | "Assumed display gamma 2.8" | historical | historical |
| 6    | See code point 1 | | |
| 7    | SMPTE ST 240 camera OETF | SMPTE ST 240 camera OETF | ? (5) |
| 8    | identity | linear | linear |
| 9    | logarithmic ($`100:1`$) | as given? (4) | inverse of as given? (4) |
| 10   | logarithmic ($`100\sqrt{10}:1`$) | as given? (4) | inverse of as given? (4) |
| 11   | extended BT.709-6 OETF ([xvYCC]) | IEC 61966-2-4 | ? (7) |
| 12   | BT.1361-0 OETF | historical | historical |
| 13   | IEC 61966-2-1 (sRGB, sYCC) | piecewise (8) | $`o = e^{2.2}`$ (8) (9) |
| 14   | See code point 1 | | |
| 15   | See code point 1 | | |
| 16   | BT.2100-2 inverse PQ EOTF | adjustments + PQ OOTF + inverse PQ EOTF | PQ EOTF (1) |
| 17   | SMPTE ST 428-1 (3) | as given? | inverse of as given? |
| 18   | BT.2100-2 HLG OETF | adjustments + HLG OETF | inverse HLG OETF + HLG OOTF (2) |

(1): Produces absolute luminance values, and requires an additional OOTF to
adapt to the actual display dynamic range. The additional OOTF practically
requires some statistics from the content or the mastering process, e.g.
static HDR metadata (SMPTE ST 2086).

(2): HLG OOTF depends on display nominal peak luminance. HLG reference EOTF
adds also a black level lift parameter.

(3): Scaled CIE XYZ values.

(4): No specification referred to in H.273.

(5): Could be either SMPTE ST 240 reproducer EOTF or BT.1886 EOTF, see
[a](https://github.com/sekrit-twc/zimg/commit/dbc745020255ec3aa910a5b9a34b242bf5c556c8),
[b](https://github.com/sekrit-twc/zimg/issues/155).

(6): Rec. ITU-R BT.1700-0 does not seem to say.

(7): No access to IEC 61966-2-4 to see how BT.1886 EOTF should be extended.

(8): The piecewise function is defined in [sRGB], with a small linear section
near zero. Regardless of what the piecewise function may have been defined for,
in practice all "sRGB" displays have been using 2.2 power-law as their EOTF.
To preserve the established look of sRGB content, decoding to display
colorimetry needs to happen with 2.2 power-law. See also [Q&A][QA:sRGB twopiece].

(9): How to handle sYCC is unclear.

Transfer characteristics are often tied to specific viewing conditions. When
actual viewing conditions differ from those, additional image adjustments may
be necessary.

It is recommended that Wayland compositors do not implement historical or
uncertain code points.

[xvYCC]: https://en.wikipedia.org/wiki/XvYCC
[sRGB]: specs.md#iec4wd-61966-2-1-default-rgb-colour-space-srgb
[QA:sRGB twopiece]: wayland_qa.md#q-should-srgb-content-be-decoded-with-the-piecewise-srgb-transfer-function


## Color models in MatrixCoefficients

`MatrixCoefficients` are defined in Rec. ITU-T H.273 through a set of
conditional equations used for encoding: you start from optical (linear) RGB
and produce RGB or YUV pixel values. A Wayland compositor will do *the inverse*
of that to decode client content. Several sets of equations apply for each code
point, encoding the values with intermediate steps. For a code point, you need
to find all the sets of equations that apply, and then stitch them together
based on what variables they operate on.

Each `MatrixCoefficients` code point supports only a specific set of color
models, usually just one. The possible color models are RGB and YCbCr.
Furthermore, color models YCgCo, Y'D'zD'x, and ICtCp have defined channel
mappings with YCbCr and XYZ with RGB. This means that there is a dependency
between pixel formats and the code point: a pixel format must have the color
channels used by the code point.

Pixel formats are divided into RGB and YUV. YUV pixel formats define the Y, Cb
and Cr color channels. With the additional channel mapping conventions defined
in Rec. ITU-T H.273 it is possible to choose compatible pixel formats for
each `MatrixCoefficients` code point, while the other pixel formats are
incompatible.

The list of compatible pixel format types for each `MatrixCoefficients` code
point are in the following table.

| code | pixel type | notes | mappings |
|------|------------|-------|----------|
| 0    | RGB and YUV | identity; sRGB, XYZ | Y↔G↔Y, Cb↔B↔Z, Cr↔R↔X |
| 1    | YUV | BT.709 | |
| 2    | unknown | unspecified | |
| 3    | - | reserved code point | |
| 4    | YUV | | |
| 5    | YUV | BT.601 625 | |
| 6    | YUV | BT.601 525 | |
| 7    | YUV | | |
| 8    | YUV(1) | YCgCo | Y↔Y, Cb↔Cg, Cr↔Co |
| 9    | YUV | BT.2020 non-const. lum. | |
| 10   | YUV | BT.2020 const. lum. (2) | |
| 11   | YUV | Y'D'zD'x | Y↔Y', Cb↔D'z, Cr↔D'x |
| 12   | YUV | (3) | |
| 13   | YUV | (2) (3) | |
| 14   | YUV | BT.2100 ICtCp (PQ, HLG) (2) | |

(1): RGB is used as an intermediate step and the end result is YCbCr
mapped to YCgCo.

(2): Requires knowledge of the transfer characteristic function before
decoding to any kind of RGB is possible.

(3): Requires knowledge of the color primaries and white point.


## Wayland protocols

There are Wayland protocol extensions to carry the information:

[viewporter] extension:
- `SampleAspectRatio, SarWidth, SarHeight` through stretching a buffer
  into its intended size measured in square pixels.

color-representation extension (in development):
- `MatrixCoefficients`
- `VideoFullRangeFlag`
- `Chroma420SampleLocType`

color-management extension (in development):
- `ColourPrimaries`
- `TransferCharacteristics`

[viewporter]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/tree/main/stable/viewporter
