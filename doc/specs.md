---
SPDX-FileCopyrightText: 2023 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)

<!-- markdownlint-disable no-emphasis-as-heading -->


# List of useful standards


## Broadcasting service (television) specifications

### [ITU-R BT.601](http://www.itu.int/rec/R-REC-BT.601)

*Studio encoding parameters of digital television for standard 4:3 and
wide-screen 16:9 aspect ratios*

Two old low resolution standard dynamic range television formats.

It defines one of the common RGB-YCbCr conversion matrices.
The 625 and 525 line variants have different primaries.

### [ITU-R BT.709](https://www.itu.int/rec/R-REC-BT.709)

*Parameter values for the HDTV standards for production and international
programme exchange*

1080p resolution standard dynamic range formats.

It has the same primaries and white point as sRGB.
It defines one of the common RGB-YCbCr conversion matrices.

### [SMPTE 170M-2004](https://www.itu.int/rec/R-REC-BT.1700)

*Television — Composite Analog Video Signal — NTSC for Studio Applications*

The link refers to ITU-R BT.1700 *Characteristics of composite video signals for
conventional analogue television systems* but both documents can be found in the
downloadable zip file.

### [ITU-R BT.1886](http://www.itu.int/rec/R-REC-BT.1886)

*Reference electro-optical transfer function for flat panel displays used in
HDTV studio production*

Defines a parameterised transfer function intended for decoding (in displays
and televisions) signals encoded with BT.601, BT.709, BT.2020 and more.
The decoding function is intentionally different from the (inverse of the)
encoding function.

### [ITU-R BT.2020](http://www.itu.int/rec/R-REC-BT.2020)

*Parameter values for ultra-high definition television systems for production
and international programme exchange*

Wide color gamut, standard dynamic range. Two different ways of RGB-YCbCr
conversion: non-constant luminance and constant luminance. The non-constant
luminance is the less complex one.

### [ITU-R BT.2035](http://www.itu.int/rec/R-REC-BT.2035)

*A reference viewing environment for evaluation of HDTV program material or
completed programmes*

Defines the reference viewing environment intended for critical viewing of
BT.601, BT.709 and BT.2020 formatted material.

### [ITU-R BT.2100](http://www.itu.int/rec/R-REC-BT.2100)

*Image parameter values for high dynamic range television for use in production
and international programme exchange*

A summary of BT.2020/PQ and BT.2020/HLG video systems for HDR.

### [SMPTE ST 2084:2014](https://ieeexplore.ieee.org/document/7291452)

*ST 2084:2014 - SMPTE Standard - High Dynamic Range Electro-Optical Transfer
Function of Mastering Reference Displays*

The perceptual quantizer (PQ) transfer function.

### [SMPTE ST 2086:2018](https://pub.smpte.org/pub/st2086/st2086-2018.pdf)

*SMPTE ST 2086:2018 Mastering Display Color Volume Metadata Supporting High
Luminance and Wide Color Gamut Images*

Mastering Display Color Volume (MDCV) metadata is a common part of
HDR static metadata.

### [SMPTE ST 2094-1:2016](https://ieeexplore.ieee.org/document/7513361)

*ST 2094-1:2016 - SMPTE Standard - Dynamic Metadata for Color Volume Transform —
Core Components*

### [SMPTE ST 2094-10:2021](https://ieeexplore.ieee.org/document/9405553)

*ST 2094-10:2021 - SMPTE Standard - Dynamic Metadata for Color Volume Transform
— Application #1*

### [SMPTE ST 2094-20:2016](https://ieeexplore.ieee.org/document/7523881)

*ST 2094-20:2016 - SMPTE Standard - Dynamic Metadata for Color Volume Transform
— Application #2*

### [SMPTE ST 2094-30:2016](https://ieeexplore.ieee.org/document/7523878)

*ST 2094-30:2016 - SMPTE Standard - Dynamic Metadata for Color Volume Transform
— Application #3*

### [SMPTE ST 2094-40:2020](https://ieeexplore.ieee.org/document/9095450)

*ST 2094-40:2020 - SMPTE Standard - Dynamic Metadata for Color Volume Transform
— Application #4*


## Graphics

<!-- vale proselint.Spelling = NO -->
### IEC/4WD 61966-2-1: default RGB colour space - sRGB
<!-- vale proselint.Spelling = YES -->

The [official specification][srgb:spec] is behind a pay-wall. A
[draft][srgb:draft] is available from the W3C.

[srgb:spec]: https://webstore.iec.ch/publication/6169
[srgb:draft]: https://www.w3.org/Graphics/Color/sRGB.html

### [Adobe RGB](https://www.adobe.com/digitalimag/pdfs/AdobeRGB1998.pdf)

*Adobe® RGB (1998) Color Image Encoding*

### SMPTE EG 432-1:2010

*SMPTE Engineering Guideline - Digital Source Processing — Color Processing for
D-Cinema*

The official specification is [behind a pay-wall][EG432:spec]. ICC has some
notes about [DCI P3][EG432:notes] defined by it.

Apple's *Display P3* is a modification of DCI P3, see [Wikipedia][DCI-P3:wiki]
and [developer documentation][DisplayP3].

[EG432:spec]: https://doi.org/10.5594/SMPTE.EG432-1.2010
[EG432:notes]: http://www.color.org/chardata/rgb/DCIP3.xalter
[DCI-P3:wiki]: https://en.wikipedia.org/wiki/DCI-P3
[DisplayP3]: https://developer.apple.com/reference/coregraphics/cgcolorspace/1408916-displayp3


## Coding standards

### [ANSI/CTA-861-I](https://shop.cta.tech/products/a-dtv-profile-for-uncompressed-high-speed-digital-interfaces-ansi-cta-861-i)

*A DTV Profile for Uncompressed High Speed Digital Interfaces*

Video signalling format definitions used by at least HDMI and
DisplayPort, including signal metadata definitions, e.g. colorimetry
selection. Defines the CTA extension of VESA E-EDID for similar metadata
describing sink capabilities.

### [Recommendation ITU-T H.273](https://handle.itu.int/11.1002/1000/14661)

*Coding-independent code points for video signal type identification*
a.k.a CICP.

Check [Usage of video signal type code points] for additional information.

See also [notes on CICP].

[Usage of video signal type code points]: https://handle.itu.int/11.1002/1000/14652
[notes on CICP]: cicp_h273.md

### [Recommendation ITU-T H.274](https://handle.itu.int/11.1002/1000/14949)

*Versatile supplemental enhancement information messages for coded video
bitstreams*

Parameterizations for everything you might need and more.
