---
SPDX-FileCopyrightText: 2023 Red Hat, Inc.
SPDX-License-Identifier: MIT
---

Contents

[[*TOC*]]

[Front page](../README.md)


# Wayland color protocols Q&A

Questions and Answers about the Wayland color-representation and
color-management protocols.


## Q: Do compositors scale surfaces with linear/tristimulus values?

A: Neither the color-representation nor the color-management protocol defines if
scaling is done on some encoded values or on tristimulus values. The behavior is
compositor defined if no other protocol defines it.

If there are good use cases which require more guarantees for the scaling
behavior one of the protocols could be extended.

Discussion: [Scaling of surfaces][CM-issue8]

[CM-issue8]: https://gitlab.freedesktop.org/swick/wayland-protocols/-/issues/8


## Q: What is the effective bit depth of the display chain?

A: There is currently no way to query the effective bit depth of the display
chain.

Linux compositors are currently unable to determine to effective bit depth of
various components in the display chain making it impossible to reason about
the effective bit depth. Display bit depth information is rarely available from
EDID and DisplayID, the bit depth on the cable and the bit depth of scanout
engines' color pipelines are not visible to user space.

If this information becomes reliably available to compositors the information
could be relayed in one of the Wayland protocols.

Discussion: [Bit depth][CM-issue7]

[CM-issue7]: https://gitlab.freedesktop.org/swick/wayland-protocols/-/issues/7

## Q: Should colorimetric rendering intents disable compositor color effects?

A: No.

Color effects like graying out a non-responsive window or a window that is
"blocked" by a modal dialog are part of the system UI design. If different
windows exhibited these UI hints differently or not at all, it would confuse the
user.

The desktop environment or compositor should allow an end user to decide whether
color effects are allowed or not in the first place.

Discussion: [Client rendering intents vs. compositor color effects][issue25]

[issue25]: https://gitlab.freedesktop.org/pq/color-and-hdr/-/issues/25


## Q: Can compositor color effects violate the perceptual rendering intent?

A: Yes.

Perceptual and saturation rendering intents have their goals that can be broken
by color effects, even if the rendering intents have no exact definition on how
they need to be implemented.

Discussion: [Client rendering intents vs. compositor color effects][issue25]


## Q: Should sRGB content be decoded with the piecewise sRGB transfer function?

A: Generally **no**.

The [sRGB] specification defines two things: the colorimetric encoding function,
and the reference display with its reference viewing conditions. The encoding
function is the piecewise transfer function, while the reference display uses
2.2 power-law (gamma 2.2) input/output RGB characteristic.

One function does not invert the other, meaning that display colorimetry will be
different from whatever was the colorimetry initially encoded. The initial
colorimetry might not have existed at all, if artists work on an image based on
how they see it on a display.

Regardless of the original intents of the specification writers, "sRGB" displays
have always decoded their signal with 2.2 power-law. That produces the look that
artists and content authors have tuned their works for and what end users
expect. Changing that is both unnecessary and would lead to complaints (e.g.
[this][win11-sRGB]).

However, there are other use cases than display. If you are editing sRGB content
and you temporarily need to decode it to light-linear and then re-encode it back
into a file, for example, the most important thing is to make sure that the
re-encoding is the exact inverse of the decoding. If you guarantee that, you can
choose between the piecewise function and the power-law.

If a compositor receives sRGB content and drives an sRGB display, it can choose
between the piecewise function and the power-law for light-linear blending
purposes. [Practicalities][lut-error] might make the piecewise function more
attractive. If there is also non-sRGB content in the same composition, things
get more complicated with the piecewise function.

The choice between the two functions defines the temporary working space: 2.2
power-law gives you intended display colorimetry, and the piecewise function
gives you some hypothetical colorimetry of who knows what.

See also: [issue](https://gitlab.freedesktop.org/pq/color-and-hdr/-/issues/12)

[sRGB]: specs.md#iec4wd-61966-2-1-default-rgb-colour-space-srgb
[win11-sRGB]: https://github.com/dylanraga/win11hdr-srgb-to-gamma2.2-icm
[lut-error]: ../plots/index.md#error-from-using-a-lut-to-implement-an-inverse-eotf
